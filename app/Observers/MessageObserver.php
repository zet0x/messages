<?php

namespace App\Observers;

use App\Models\Message;
use App\Events\CreateMessageEvent;

class MessageObserver
{
    public function created(Message $message){
    	event(new CreateMessageEvent($message));
    }
}
