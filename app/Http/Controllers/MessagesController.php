<?php

namespace App\Http\Controllers;

use Validator;
use Illuminate\Http\Request;
use App\Models\Message;

class MessagesController extends Controller
{
    public function index(Request $request){
		$messages = Message::latest()->get();

		return response()->json([
            'status' => "ok",
            'data' => $messages
        ]);

    }

    public function create(Request $request){
    	$input = $request->all();
    	$validator = Validator::make($input, [
            'name' => 'required|string',
            'text' => 'required|string'
        ]);
        if ($validator->fails()) {
            return response()->json([
                'status' => "error",
                'description' => 'Ошибка!'
            ]);
        }

        $message = Message::create($input);

        return response()->json([
            'status' => "ok",
            'data' => $message
        ]);
    }
}
