@extends('layouts.template')

@section('title')
    {{ config()->get('app.name') }}
@endsection

@section('content')

    <component :is="layout"></component>

@endsection
