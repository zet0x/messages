require('./bootstrap');

import gsap from 'gsap';
import VueEcho from 'vue-echo-laravel';
import moment from 'moment';
import VueNotification from "@kugatsu/vuenotification";
import Vuex from 'vuex'

Vue.use(Vuex)

Vue.use(VueEcho, {
   broadcaster: 'socket.io',
   host: 'http://ovz2.dvurechensky48.pq4yn.vps.myjino.ru:49161',
   encrypted: true,
   logToConsole: true,
   auth: {
    headers: {

    }
   },
   authEndpoint: "/broadcasting/auth"
});

Vue.use(VueNotification, {
  showCloseIcn: true,
  timer: 8
});


moment.locale('ru')
Vue.prototype.$moment = moment;

const store = new Vuex.Store({
	state: {
		name: null
	},
	mutations: {
		setName(state, name){
			state.name = name;
		}
	},
	actions: {
		setName({commit}, name){
			commit('setName', name);
		}
	},
	getters: {
		name(state){
			return state.name;
		}
	}
})

//LAYOUTS
Vue.component('name-layout', require('./layouts/NameLayout.vue').default);
Vue.component('message-layout', require('./layouts/MessageLayout.vue').default);

// MESSAGES WIDGETS
Vue.component('messages-widget', require('./widgets/MessagesWidget.vue').default);
Vue.component('create-message-widget', require('./widgets/CreateMessageWidget.vue').default);


const App = new Vue({
	store,
	el: "#app",
	data: function(){
		return {
			
		}
	},
	methods: {
		init(){
			var app = this;
		}
	},
	mounted: function(){
		this.init();
	},
	computed: {
		name: {
			get(){
	            return this.$store.state.name;
	        },
	        set(value){
	          this.$store.dispatch('setName', value);
	        }
		},
		layout: function(){
			var app = this;
			if(app.name == null){
				return 'name-layout';
			}
			else{
				return 'message-layout'; 
			}
		}
	}
});